module com.cezkor.zad2_io {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.cezkor.zad2_io to javafx.fxml;
    exports com.cezkor.zad2_io;
    exports com.cezkor.zad2_io.animalscontrollers;

}