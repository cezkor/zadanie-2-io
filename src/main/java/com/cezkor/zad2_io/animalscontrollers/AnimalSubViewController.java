package com.cezkor.zad2_io.animalscontrollers;

import com.cezkor.zad2_io.animals.Animal;
public class AnimalSubViewController {

    protected Animal animal;

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

    public void saveDataFromViewToAnimal() {
        // w glownym widoku edycji jest to czynione
    }
}
