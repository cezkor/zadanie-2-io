package com.cezkor.zad2_io.animalscontrollers;

import com.cezkor.zad2_io.animals.Animal;
import com.cezkor.zad2_io.animals.Shark;
import javafx.fxml.Initializable;
import javafx.scene.control.Spinner;

import java.net.URL;
import java.util.ResourceBundle;

public class SharkSubViewController extends AnimalSubViewController implements Initializable {

    public Spinner<Integer> teethCountSpinner;

    private Shark shark;

    @Override
    public void setAnimal(Animal animal) {
        super.setAnimal(animal);
        if (! (animal instanceof Shark)){
            throw new IllegalArgumentException("Provided Animal is not a Shark");
        }
        shark = (Shark) animal;
    }

    @Override
    public void saveDataFromViewToAnimal() {
        shark.setTeethCount(teethCountSpinner.getValue());
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        teethCountSpinner.getEditor().textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) { // nie dopuszczam by ktos wpisal do spinnera cos innego niz cyfry
                teethCountSpinner.getEditor().setText(oldValue);
            }
        });

    }

}
