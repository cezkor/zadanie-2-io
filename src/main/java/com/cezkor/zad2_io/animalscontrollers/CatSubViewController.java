package com.cezkor.zad2_io.animalscontrollers;

import com.cezkor.zad2_io.animals.Animal;
import com.cezkor.zad2_io.animals.Cat;
import com.cezkor.zad2_io.animals.Dog;
import javafx.fxml.Initializable;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;

import java.net.URL;
import java.util.ResourceBundle;

public class CatSubViewController extends AnimalSubViewController implements Initializable {

    public TextField furColorNameTextField;
    public TextField breedNameTextField;
    public Spinner<Integer> catLivesSpinner;

    private Cat cat;

    @Override
    public void setAnimal(Animal animal) {
        super.setAnimal(animal);
        if (! (animal instanceof Cat)){
            throw new IllegalArgumentException("Provided Animal is not a Cat");
        }
        cat = (Cat) animal;
    }

    @Override
    public void saveDataFromViewToAnimal() {
        cat.setCatLivesCount(catLivesSpinner.getValue());
        cat.setBreedName(breedNameTextField.getText());
        cat.setFurColorName(furColorNameTextField.getText());
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        catLivesSpinner.getEditor().textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) { // nie dopuszczam by ktos wpisal do spinnera cos innego niz cyfry
                catLivesSpinner.getEditor().setText(oldValue);
            }
        });

        // ograniczenia na dlugosci i obcinanie pustych znakow
        breedNameTextField.focusedProperty().addListener( (obs,o,n) -> {
            if (n == null || !n){
                breedNameTextField.setText(breedNameTextField.getText().trim());
            }
        });
        breedNameTextField.setTextFormatter( new TextFormatter<>(change -> {

            if (  change.getControlNewText().matches(".{0," + 100 + "}") ){
                return change;
            }else {
                return null;
            }
        }));
        furColorNameTextField.focusedProperty().addListener( (obs,o,n) -> {
            if (n == null || !n){
                furColorNameTextField.setText(furColorNameTextField.getText().trim());
            }
        });
        furColorNameTextField.setTextFormatter( new TextFormatter<>(change -> {

            if (  change.getControlNewText().matches(".{0," + 100 + "}") ){
                return change;
            }else {
                return null;
            }
        }));

    }
}
