package com.cezkor.zad2_io.animalscontrollers;

import com.cezkor.zad2_io.animals.Animal;
import com.cezkor.zad2_io.animals.Cat;
import com.cezkor.zad2_io.animals.Turtle;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.util.StringConverter;

import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

public class TurtleSubViewController extends AnimalSubViewController implements Initializable {

    static private final String FRESHWATER_TURTLE = "Żółw słodkowodny";
    static private final String SEAWATER_TURTLE = "Żółw słonowodny/morski";
    static private final String LAND_TURTLE = "Żółw lądowy";

    public TextField shellColorNameTextField;
    public ComboBox<String> turtleTypeComboBox;
    public Slider shellHardnessSlider;
    private Turtle turtle;

    @Override
    public void setAnimal(Animal animal) {
        super.setAnimal(animal);
        if (! (animal instanceof Turtle)){
            throw new IllegalArgumentException("Provided Animal is not a Turtle");
        }
        turtle = (Turtle) animal;
    }

    @Override
    public void saveDataFromViewToAnimal() {
        switch (turtleTypeComboBox.getSelectionModel().getSelectedItem()){
            case SEAWATER_TURTLE -> {
                turtle.setType(Turtle.TurtleType.SEAWATER);
            }
            case LAND_TURTLE -> {
                turtle.setType(Turtle.TurtleType.LAND);
            }
            default -> {
                turtle.setType(Turtle.TurtleType.FRESHWATER);
            }
        }
        turtle.setColorOfShellName(shellColorNameTextField.getText());
        turtle.setShellHardnessInMohsScale(shellHardnessSlider.getValue());
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        turtleTypeComboBox.getItems().add(FRESHWATER_TURTLE);
        turtleTypeComboBox.getItems().add(SEAWATER_TURTLE);
        turtleTypeComboBox.getItems().add(LAND_TURTLE);
        turtleTypeComboBox.getSelectionModel().selectFirst();

        // ograniczenia na dlugosci i obcinanie pustych znakow
        shellColorNameTextField.focusedProperty().addListener( (obs,o,n) -> {
            if (n == null || !n){
                shellColorNameTextField.setText(shellColorNameTextField.getText().trim());
            }
        });
        shellColorNameTextField.setTextFormatter( new TextFormatter<>(change -> {

            if (  change.getControlNewText().matches(".{0," + 100 + "}") ){
                return change;
            }else {
                return null;
            }
        }));

    }
}
