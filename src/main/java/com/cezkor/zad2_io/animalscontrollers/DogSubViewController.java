package com.cezkor.zad2_io.animalscontrollers;

import com.cezkor.zad2_io.animals.Animal;
import com.cezkor.zad2_io.animals.Dog;
import javafx.fxml.Initializable;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;

import java.net.URL;
import java.util.ResourceBundle;

public class DogSubViewController extends AnimalSubViewController implements Initializable {

    public TextField furColorNameTextField;
    public TextField breedNameTextField;
    private Dog dog;


    @Override
    public void setAnimal(Animal animal) {
        super.setAnimal(animal);
        if (! (animal instanceof Dog)){
            throw new IllegalArgumentException("Provided Animal is not a Dog");
        }
        dog = (Dog) animal;
    }

    @Override
    public void saveDataFromViewToAnimal() {
        dog.setBreedName(breedNameTextField.getText());
        dog.setFurColorName(furColorNameTextField.getText());
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // ograniczenia na dlugosci i obcinanie pustych znakow
        breedNameTextField.focusedProperty().addListener( (obs,o,n) -> {
            if (n == null || !n){
                breedNameTextField.setText(breedNameTextField.getText().trim());
            }
        });
        breedNameTextField.setTextFormatter( new TextFormatter<>(change -> {

            if (  change.getControlNewText().matches(".{0," + 100 + "}") ){
                return change;
            }else {
                return null;
            }
        }));
        furColorNameTextField.focusedProperty().addListener( (obs,o,n) -> {
            if (n == null || !n){
                furColorNameTextField.setText(furColorNameTextField.getText().trim());
            }
        });
        furColorNameTextField.setTextFormatter( new TextFormatter<>(change -> {

            if (  change.getControlNewText().matches(".{0," + 100 + "}") ){
                return change;
            }else {
                return null;
            }
        }));
    }
}
