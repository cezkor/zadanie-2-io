package com.cezkor.zad2_io.animalstates;

import com.cezkor.zad2_io.animalscontrollers.AnimalSubViewController;
import javafx.scene.Node;
import javafx.scene.layout.VBox;

import java.io.IOException;

public interface SubViewState {

    public AnimalSubViewController handle(VBox vBoxToAttachSubViewTo) throws IOException;

}
