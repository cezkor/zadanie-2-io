package com.cezkor.zad2_io.animalstates;

import com.cezkor.zad2_io.AnimalApplication;
import com.cezkor.zad2_io.animalscontrollers.AnimalSubViewController;
import com.cezkor.zad2_io.animalscontrollers.SharkSubViewController;
import com.cezkor.zad2_io.animalscontrollers.TurtleSubViewController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.util.Random;

public class TurtleState implements SubViewState{
    @Override
    public AnimalSubViewController handle(VBox vBoxToAttachSubViewTo) throws IOException {

        FXMLLoader loader = new FXMLLoader(AnimalApplication.class.getResource("animaladdviews/turtle-sub-view.fxml"));
        Parent parent = loader.load();

        Random rand = new Random();
        ((TurtleSubViewController)loader.getController()).shellHardnessSlider.setValue(rand.nextDouble()*9 + 1);

        vBoxToAttachSubViewTo.getChildren().add(parent);
        return loader.getController();
    }
}
