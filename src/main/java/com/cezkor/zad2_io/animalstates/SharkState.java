package com.cezkor.zad2_io.animalstates;

import com.cezkor.zad2_io.AnimalApplication;
import com.cezkor.zad2_io.animalscontrollers.AnimalSubViewController;
import com.cezkor.zad2_io.animalscontrollers.CatSubViewController;
import com.cezkor.zad2_io.animalscontrollers.SharkSubViewController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.util.Random;

public class SharkState implements SubViewState{
    @Override
    public AnimalSubViewController handle(VBox vBoxToAttachSubViewTo) throws IOException {

        FXMLLoader loader = new FXMLLoader(AnimalApplication.class.getResource("animaladdviews/shark-sub-view.fxml"));
        Parent parent = loader.load();

        Random rand = new Random();
        ((SharkSubViewController)loader.getController()).teethCountSpinner.getEditor().setText("" + (1 + rand.nextInt()%32));

        vBoxToAttachSubViewTo.getChildren().add(parent);
        return loader.getController();
    }
}
