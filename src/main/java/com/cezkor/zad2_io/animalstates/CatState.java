package com.cezkor.zad2_io.animalstates;

import com.cezkor.zad2_io.AnimalApplication;
import com.cezkor.zad2_io.animalscontrollers.AnimalSubViewController;
import com.cezkor.zad2_io.animalscontrollers.CatSubViewController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.VBox;

import java.io.IOException;

public class CatState implements SubViewState{
    @Override
    public AnimalSubViewController handle(VBox vBoxToAttachSubViewTo) throws IOException {

        FXMLLoader loader = new FXMLLoader(AnimalApplication.class.getResource("animaladdviews/cat-sub-view.fxml"));
        Parent parent = loader.load();

        ((CatSubViewController)loader.getController()).catLivesSpinner.getEditor().setText("9");

        vBoxToAttachSubViewTo.getChildren().add(parent);
        return loader.getController();
    }
}
