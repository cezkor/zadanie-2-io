package com.cezkor.zad2_io.animalstates;

import com.cezkor.zad2_io.AnimalApplication;
import com.cezkor.zad2_io.animalscontrollers.AnimalSubViewController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.VBox;

import java.io.IOException;

public class AnimalState implements SubViewState{
    @Override
    public AnimalSubViewController handle(VBox vBoxToAttachSubViewTo) throws IOException {

        FXMLLoader loader = new FXMLLoader(AnimalApplication.class.getResource("animaladdviews/animal-sub-view.fxml"));
        Parent parent = loader.load();

        vBoxToAttachSubViewTo.getChildren().add(parent);
        return loader.getController();
    }
}
