package com.cezkor.zad2_io;

import com.cezkor.zad2_io.animals.*;
import com.cezkor.zad2_io.animalscontrollers.AnimalSubViewController;
import com.cezkor.zad2_io.animalstates.*;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class AddAnimalViewController implements Initializable {

    public VBox changeableVBox;

    static private class AnimalTypesStrings {
        public final static String CAT = "Kot";
        public final static String DOG = "Pies";
        public final static String SHARK = "Rekin";
        public final static String TURTLE = "Żółw";
        public final static String ANIMAL = "Zwierzę";
    }

    public Button addAnimalButton;
    public Button cancelButton;
    public TextField nameTextField;
    public Spinner<Integer> ageSpinner;
    public ComboBox<String> animalTypeComboBox;

    private Animal newAnimal = new Animal("Zwierzę", 0);

    private boolean doesUserWantToSave = false;

    private SimpleBooleanProperty doesUserWantToLeave = new SimpleBooleanProperty(false);

    private AnimalSubViewController subViewController;

    private SubViewGenerator subViewGenerator = new SubViewGenerator();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        Platform.runLater( () -> {
            addAnimalButton.getScene().getWindow().setOnCloseRequest( ev -> {
                ev.consume();
                attemptLeavingWithoutSaving();
            });
        });

        ageSpinner.getEditor().textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) { // nie dopuszczam by ktos wpisal do spinnera cos innego niz cyfry
                ageSpinner.getEditor().setText(oldValue);
            }
        });

        nameTextField.focusedProperty().addListener( (obs,o,n) -> {
            if (n == null || !n){
                nameTextField.setText(nameTextField.getText().trim());
            }
        });
        nameTextField.setTextFormatter( new TextFormatter<>(change -> {

            if (  change.getControlNewText().matches(".{0," + 100 + "}") ){
                return change;
            }else {
                return null;
            }
        }));

        animalTypeComboBox.getItems().add(AnimalTypesStrings.ANIMAL);
        animalTypeComboBox.getItems().add(AnimalTypesStrings.CAT);
        animalTypeComboBox.getItems().add(AnimalTypesStrings.DOG);
        animalTypeComboBox.getItems().add(AnimalTypesStrings.SHARK);
        animalTypeComboBox.getItems().add(AnimalTypesStrings.TURTLE);

        animalTypeComboBox.setOnAction( ev -> { // ustawianie widoku w zaleznosci od rodzaju zwierzecia
            switch (animalTypeComboBox.getSelectionModel().getSelectedItem()){
                case AnimalTypesStrings.DOG -> {
                    subViewGenerator.setState(new DogState());
                    newAnimal = new Dog("Pies",0,"","");
                }
                case AnimalTypesStrings.CAT -> {
                    subViewGenerator.setState(new CatState());
                    newAnimal = new Cat("Kot",0,"","",9);
                }
                case AnimalTypesStrings.SHARK -> {
                    subViewGenerator.setState(new SharkState());
                    newAnimal = new Shark("Rekin",0,0);
                }
                case AnimalTypesStrings.TURTLE -> {
                    subViewGenerator.setState(new TurtleState());
                    newAnimal = new Turtle("Żółw",0,"", 10, Turtle.TurtleType.FRESHWATER);
                }
                default -> {
                    subViewGenerator.setState(new AnimalState());
                    newAnimal = new Animal("Zwierzę",0);
                }
            }

            try {
                subViewController = subViewGenerator.generateSubView(changeableVBox);
                subViewController.setAnimal(newAnimal);
            }
            catch (Exception e){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Błąd");
                alert.setHeaderText("Błąd");
                alert.setContentText("Błąd: " + e.getMessage());
                alert.showAndWait();
            }
        });

        ageSpinner.getEditor().setText("0");
        nameTextField.setText("Zwierzę");
        animalTypeComboBox.getSelectionModel().selectFirst();
        animalTypeComboBox.fireEvent(new ActionEvent());
    }

    public SimpleBooleanProperty doesUserWantToLeaveProperty() {
        return doesUserWantToLeave;
    }

    public void addAnimal(ActionEvent actionEvent) {

        newAnimal.setName(nameTextField.getText());
        newAnimal.setAge(ageSpinner.getValue());
        subViewController.saveDataFromViewToAnimal();

        doesUserWantToSave = true;
        doesUserWantToLeave.setValue(true);
    }

    public void cancel(ActionEvent actionEvent) {
        attemptLeavingWithoutSaving();
    }

    private void attemptLeavingWithoutSaving() {
        ButtonType yesBT = new ButtonType("Tak");
        ButtonType noBT = new ButtonType("Nie");

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"",yesBT,noBT);
        alert.setTitle("Potwierdzenie");
        alert.setHeaderText("Opuszczenie okna dodawania");
        alert.setContentText("Czy na pewno chcesz anulować dodawanie zwierzęcia? NIE ZOSTANIE ONO ZAPISANE!");

        alert.showAndWait();

        if (alert.getResult().getText().equals("Tak")){
            doesUserWantToLeave.setValue(true);
        }


    }

    public Animal getNewAnimal() {
        return newAnimal;
    }

    public boolean doesUserWantToSave() {
        return doesUserWantToSave;
    }
}
