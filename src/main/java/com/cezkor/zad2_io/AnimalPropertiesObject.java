package com.cezkor.zad2_io;

import com.cezkor.zad2_io.animals.*;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class AnimalPropertiesObject {

    public Animal getAnimal() {
        return animal;
    }

    private Animal animal;

    private StringProperty name = new SimpleStringProperty("Imię");

    private StringProperty typeOfAnimal = new SimpleStringProperty("Zwierzę");

    public AnimalPropertiesObject(Animal animal) {
        this.animal = animal;

        if (animal.getName() != null) {
            name.setValue(animal.getName());
        }

        if (animal instanceof Cat) { typeOfAnimal.setValue("Kot"); }
        if (animal instanceof Dog) { typeOfAnimal.setValue("Pies"); }
        if (animal instanceof Shark) { typeOfAnimal.setValue("Rekin"); }
        if (animal instanceof Turtle) { typeOfAnimal.setValue("Żółw"); }

    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public String getTypeOfAnimal() {
        return typeOfAnimal.get();
    }

    public StringProperty typeOfAnimalProperty() {
        return typeOfAnimal;
    }
}
