package com.cezkor.zad2_io;

import com.cezkor.zad2_io.animals.*;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

public class MainViewController implements Initializable {


    public Button addAnimalButton;
    public TableView mainTableView;
    public TableColumn<Object, Object> animalNameColumn;
    public TableColumn<Object, Object> animalTypeColumn;
    public Button saveAndExitButton;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        animalNameColumn.setCellValueFactory(
                new PropertyValueFactory<>("name")
        );
        animalTypeColumn.setCellValueFactory(
                new PropertyValueFactory<>("typeOfAnimal")
        );
        mainTableView.setPlaceholder(new Label("Naciśnij \"Dodaj zwierzę\" by zapełnić listę"));

        Platform.runLater(() -> { // gdy uzytkownik wcisnie X
            Stage stage = (Stage) saveAndExitButton.getScene().getWindow();
            stage.setOnCloseRequest( windowEvent -> {
                windowEvent.consume();

                ButtonType yesBT = new ButtonType("Tak");
                ButtonType noBT = new ButtonType("Nie");

                Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"",yesBT,noBT);
                alert.setTitle("Potwierdzenie");
                alert.setHeaderText("Opuszczenie programu");
                alert.setContentText
                        ("Czy na pewno chcesz opuścić program bez zapisania listy? NIE ZOSTANIE ONA ZAPISANA!");

                alert.showAndWait();

                if (alert.getResult().getText().equals("Tak")){
                    Platform.exit();
                }
            });

        });

    }

    public void onAddAnimal(ActionEvent actionEvent) {

        try {
            FXMLLoader fxmlLoader = new FXMLLoader(AnimalApplication.class.getResource("add-view.fxml"));
            Scene subScene = new Scene(fxmlLoader.load(), 400, 340);
            Stage subStage = new Stage();
            subStage.setScene(subScene);
            subStage.setResizable(false);
            subStage.setTitle("Dodaj zwierzę");

            AddAnimalViewController controller = fxmlLoader.getController();

            controller.doesUserWantToLeaveProperty().addListener( (obs,o,n) -> {
                if (n != null && n) {
                    if (controller.doesUserWantToSave()){
                        mainTableView.getItems().add(new AnimalPropertiesObject(controller.getNewAnimal()));
                    }
                    subStage.close();
                }
            });

            subStage.show();

        }
        catch (Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Błąd");
            alert.setTitle("Błąd");
            alert.setContentText("Błąd: " + e.getMessage());
            alert.showAndWait();
        }
    }

    public void saveAndExit(ActionEvent actionEvent) {
        Stage stage = (Stage) saveAndExitButton.getScene().getWindow();
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().clear();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Pliki tekstowe", "*.txt"));
        fileChooser.setInitialFileName("animals.txt");

        File fileToSave = fileChooser.showSaveDialog(stage);
        if (fileToSave == null) return;

        try (FileWriter fw = new FileWriter( fileToSave );
             BufferedWriter bw = new BufferedWriter( fw );) {

            for (Object apo : mainTableView.getItems()) {
                AnimalPropertiesObject a = (AnimalPropertiesObject) apo;
                System.out.println(a.getAnimal().toString());
                bw.write(a.getAnimal().toString());
                bw.newLine();
            }

            Platform.exit();
        }
        catch (Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Błąd");
            alert.setTitle("Błąd");
            alert.setContentText("Błąd: " + e.getMessage());
            alert.showAndWait();
        }
    }
}
