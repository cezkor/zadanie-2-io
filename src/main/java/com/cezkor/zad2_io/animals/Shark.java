package com.cezkor.zad2_io.animals;

public class Shark extends Animal{

    private long teethCount;

    public Shark(String name, Integer age, long teethCount) {
        super(name, age);
        this.teethCount = teethCount;
    }

    public long getTeethCount() {
        return teethCount;
    }

    public void setTeethCount(long teethCount) {
        this.teethCount = teethCount;
    }

    @Override
    public String toString() {
        return "Shark{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", teethCount=" + teethCount +
                '}';
    }
}
