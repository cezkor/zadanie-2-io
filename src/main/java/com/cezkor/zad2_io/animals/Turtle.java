package com.cezkor.zad2_io.animals;

public class Turtle extends Animal{

    public enum TurtleType{
        FRESHWATER,SEAWATER,LAND
    }
    private String colorOfShellName;

    public String getColorOfShellName() {
        return colorOfShellName;
    }

    public void setColorOfShellName(String colorOfShellName) {
        this.colorOfShellName = colorOfShellName;
    }

    public double getShellHardnessInMohsScale() {
        return shellHardnessInMohsScale;
    }

    public void setShellHardnessInMohsScale(double shellHardnessInMohsScale) {
        this.shellHardnessInMohsScale = shellHardnessInMohsScale;
    }

    public TurtleType getType() {
        return type;
    }

    public void setType(TurtleType type) {
        this.type = type;
    }

    private double shellHardnessInMohsScale;

    private TurtleType type;

    public Turtle(String name, Integer age, String colorOfShellName, double shellHardnessInMohsScale, TurtleType type) {
        super(name, age);
        this.colorOfShellName = colorOfShellName;
        this.shellHardnessInMohsScale = shellHardnessInMohsScale;
        this.type = type;
    }

    @Override
    public String toString() {
        return "Turtle{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", colorOfShellName='" + colorOfShellName + '\'' +
                ", shellHardnessInMohsScale=" + shellHardnessInMohsScale +
                ", type=" + type +
                '}';
    }
}
