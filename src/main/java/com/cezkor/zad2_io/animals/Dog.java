package com.cezkor.zad2_io.animals;

public class Dog extends Animal{

    private String breedName;

    private String furColorName;

    public Dog(String name, Integer age, String breedName, String furColorName) {
        super(name, age);
        this.breedName = breedName;
        this.furColorName = furColorName;
    }

    public String getBreedName() {
        return breedName;
    }

    public void setBreedName(String breedName) {
        this.breedName = breedName;
    }

    public String getFurColorName() {
        return furColorName;
    }

    public void setFurColorName(String furColorName) {
        this.furColorName = furColorName;
    }

    @Override
    public String toString() {
        return "Dog{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", breedName='" + breedName + '\'' +
                ", furColorName='" + furColorName + '\'' +
                '}';
    }
}
