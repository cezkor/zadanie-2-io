package com.cezkor.zad2_io.animals;

public class Cat extends Animal{

    private String breedName;

    private String furColorName;

    private long catLivesCount;

    public Cat(String name, Integer age, String breedName, String furColorName, long catLivesCount) {
        super(name, age);
        this.breedName = breedName;
        this.furColorName = furColorName;
        this.catLivesCount = catLivesCount;
    }

    public String getBreedName() {
        return breedName;
    }

    public void setBreedName(String breedName) {
        this.breedName = breedName;
    }

    public String getFurColorName() {
        return furColorName;
    }

    public void setFurColorName(String furColorName) {
        this.furColorName = furColorName;
    }

    public long getCatLivesCount() {
        return catLivesCount;
    }

    public void setCatLivesCount(long catLivesCount) {
        this.catLivesCount = catLivesCount;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", breedName='" + breedName + '\'' +
                ", furColorName='" + furColorName + '\'' +
                ", catLivesCount=" + catLivesCount +
                '}';
    }
}
