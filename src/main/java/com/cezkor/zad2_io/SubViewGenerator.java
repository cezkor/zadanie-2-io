package com.cezkor.zad2_io;

import com.cezkor.zad2_io.animalscontrollers.AnimalSubViewController;
import com.cezkor.zad2_io.animalstates.AnimalState;
import com.cezkor.zad2_io.animalstates.SubViewState;
import javafx.scene.control.Alert;
import javafx.scene.layout.VBox;

import java.io.IOException;

public class SubViewGenerator { // we wzorcu Stanu: context

    private SubViewState state = new AnimalState();

    public void setState(SubViewState state) {
        if (state != null) this.state = state;
    }

    public AnimalSubViewController generateSubView(VBox vBoxToAttachSubViewTo) throws IOException {
        vBoxToAttachSubViewTo.getChildren().clear();
        return state.handle(vBoxToAttachSubViewTo);
    }
}
